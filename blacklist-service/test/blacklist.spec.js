const blacklist = require("../blacklist");

describe("Testing Blacklist service", () => {
  test("it should return true for registration AA123AA", () => {
    const expected = ["rule::registernumber::blacklist"];

    return blacklist
      .handler({
        registerNumber: "AA123AA",
      })
      .then((scams) => {
        expect(scams).toStrictEqual(expected);
      });
  });

  test("it should return un empty scams array for registration ES921VR (not in black list)", () => {
    const expected = [];

    return blacklist
      .handler({
        registerNumber: "ES921VR",
      })
      .then((scams) => {
        expect(scams).toStrictEqual(expected);
      });
  });

  test("it should throw an error when registration is undefined", async () => {
    const input = { registerNumber: undefined };
    const expected = new Error("Registrations is not defined");

    try {
      await blacklist.handler(input);
    } catch (error) {
      expect(error).toStrictEqual(expected);
    }
  });
});

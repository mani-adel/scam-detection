/**
 * ex:
 * @param {JSON} event
 */
exports.handler = async (event) => {
  const registration = event.registerNumber;

  return isBlackListed(registration).then((isInBlacklist) => {
    if (isInBlacklist) {
      return ["rule::registernumber::blacklist"];
    }
    return [];
  });
};

/**
 *
 * @param {String} registration
 */
function isBlackListed(registration) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (registration) {
        const blacklist = ["AA123AA"];
        const check = blacklist.includes(registration);
        resolve(check);
      } else {
        reject(new Error("Registrations is not defined"));
      }
    }, 50);
  });
}

# Scams detection 

Ce projet permet de détecter les aranques liées aux publications d'annonces, comme indqué dans la spécification du besoin projet [@home - CT_01](./doc/Test-technique-tout-langage-CT01.pdf)

# Les outils de développement
- Visuel studio code: éditeur de codes 
- NodeJS : runtime javascript coté serveur
- NPM: Gestion de dépendances de paxkages / librairies
- Eslint / Prettier : unifier le code des différents éditeurs et IDEs des différents développeurs et détecter des problème dans l'implémentation (variable déclarées non utilisée, ...) et corriger certains automatiquement.
- Serveless framework : permet de créer des apps sans serveur, les déployer facilement sur aws, les monitorer via le dahboard (facilitateur de développement)
- Jest: frame work de test unitaire. 
- TDD: méthodologie/approche permettant aux tests de guider les développements
- Git : Gère le versionning du code
- Docker (pas utilisé dans ce projet): facilite le déploiement et un moyen pour s'assurer que les environements sont équivaux (dans notre cas avoir la même version node, ..) 
- environnement CI/CD (pas utilisé dans ce code): gitlab-ci, bitbucketpipline, ... permettant d'assurer d'une façon automatique le déploiement continu et l'intégration continue. Dans notre cas, le pipline servira à remplir les variable d'env depuis bitbucket (pour serverless.yml, variable d'accès, ...) et de lancer l'exécution du fichier serverless.yml 
- Un compte AWS: Déployer tester, valider, ...
- Un compte serverless : facilite le monitoring des applications serverless.

# La structure du projet  

```shell
scam-detection
├── node_modules
├── Package.json
├── serverless.yml
├── blacklist-service
│   ├── test
│   ├── package.json   
│   └── blacklist.s
│
├── quotation-service  
│   ├── test             
│   ├── package.json           
│   └── quotation.js     
├── contact-service  
│   ├── test             
│   ├── package.json           
│   └── contact.js 
├──  init 
│   ├── init-withoutSM.js (Orchestrateur des 3 lambdas)          
│   ├── init.js  ( initie la step function)
│   └── package.json
├──  libs (common code)
│   ├──  test
│   ├──  validator.js (used for name, firstname ans email)
│   ├──  registrationValidator.js (extends validator)
│   └──  quotationValidator.js (extends validator)
│                   
└── ...
```
# couverture de test


File                    | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s 
------------------------|---------|----------|---------|---------|-------------------
All files               |     100 |    90.48 |     100 |     100 |                   
 blacklist-service      |     100 |      100 |     100 |     100 |                   
  blacklist.js          |     100 |      100 |     100 |     100 |                   
 build-response         |     100 |    66.67 |     100 |     100 |                   
  buildresponse.js      |     100 |    66.67 |     100 |     100 | 2-4,12-19         
 contact-service        |     100 |      100 |     100 |     100 |                   
  contact.js            |     100 |      100 |     100 |     100 |                   
 libs                   |     100 |      100 |     100 |     100 |                   
  quotationValidator.js |     100 |      100 |     100 |     100 |                   
  validator.js          |     100 |      100 |     100 |     100 |                   
 quotation-service      |     100 |      100 |     100 |     100 |                   
  quotation.js          |     100 |      100 |     100 |     100 |                   
------------------------|---------|----------|---------|---------|-------------------
Test Suites: 6 passed, 6 total
Tests:       23 passed, 23 total


# Installation 

```sh
$ git clone repo
$ cd scam-detection
$ npm install 
```

# Execution en local (sans State Machine/step function)  

Comme vous le constatez, le projet est développé sur les bases du framework serveless. L'exécution en local ne nécessite aucun déploiement sur AWS. 

Une state machine a été implémentée afin de proposer une solution un peu plus poussée du workflow de validation d'annonce. (sujet à dévelpper dans les prochaines sections). 
 
 
Pour lancer l'exécution, ll suffit juste de lancer la fonction init-withoutSM, qui est en charge de l'orchestration du workflow.


```sh
$ node init/init-withoutSM.js 
```

```sh
$ sls invoke local -f init-withoutSM
```

les valeurs de l'event sont dans le même fichier. 


### Architecture avec Sate Machine/step function

Je ne voulais pas m'arrêter à l'exécution `init-withoutSM.js` vu que le use case du projet est intéressant. En analysant la problèmatique, l'application doit faire des appels à des services externes, il pourrait donc y avoir des erreurs liées à des problèmes réseau, service momentanément indisponible, etc. Notre system aurait donc besoin de faire des `retry`. 
L'application doit répartir ses tâches en plusieurs services, dans notre cas en plusieurs fonctions, puisque que le projet est initié avec serverless framework, donc avec les lambdas. Il faut donc gérer et organiser ces lambdas,     

J'ai donc pensé à une orchestration avancée du workflow en intégrant les `AWS Step Functions`. Les States Machines permettent de créer des workflow, d'affecter des ressources à chaque étape, de lancer des retry en cas d'echec, et de garder en mémoire l'exécution des flux pendant 1 an. 

Avec les step functions, il est par fois facile à suivre l'évolution du besoin. A titre d'exemple: rajouter une file SQS (simple queue service) pour répértorier les fraudes. Celle-ci (File SQS)  sera dédiée à une application utilisée par le département juridique, ... 

NB: Le lancement en local avec `serverless-step-functions-offline` ne permet pas de simmuler localement les resultats du traitement `ResultPath`  (ex: le résultats des fonctions permettant de formater la réponse). 

## Schéma macro  

[![Architecture](./doc/Architecture.png)](./doc/Architecture.png)

## Description 

Dans mon scénario, j'ai choisi de lancer la state machine en passant par une API gateway car nous attendant un retour de notre traitement. Nous pouvons aussi exécuter la state Machine sans passer par la lambda INIT, c'est donc écouter les autres events: sns, sqs, kinesis, cloudwatch ... et cela en `respectant le format input de la state machine`. Le résultat sera envoyé vers une ou plusieurs files SQS ou SNS (ex: si la valeur du champ scam est true envoyer vers SQS-SCAM)

L'api Gateway reçoit des appels POST, qu'elle transfère la lambda init (voir Format event).
La lambda lance l'exécusion et marque un arrêt (sleep == timeout de la stateMachine) car la state machine est exécutée en asynchrone, sans le timeout le retour sera incomplet (status=running)

La state machine recoit la notfication, prépare la configuration pour chaque lambda: 
 
 - contact
 - blacklist
 - quotation
 
qu'elle exécute en parallèle (possibilité de retry, send to sqs, sns, ...).

Une fois que les trois fonctions citées ci-dessus, leurs résultats sera transmis à la fonction build-result, qui elle reformart les réponses tout en respectant le format définit dans le document [@home - CT_01](./doc/Test-technique-tout-langage-CT01.pdf)


### Format event:

```json 
{
        "contacts": {
            "firstName": "C",
            "lastName": "D",
            "email": "testdepot@yopmail.fr",
            "phone1": {
                "value": "0607080901"
            }
        },
        "creationDate": "2020-01-09T09:02:22.610Z",
        "price": 19000,
        "publicationOptions": ["STRENGTHS", "BOOST_VO"],
        "reference": "this is a static ",
        "vehicle": {
            "make": "HONDA",
            "model": "CR-V",
            "version": "IV (2) 1.6 I-DTEC 160 4WD EXCLUSIVE NAVI AT",
            "category": "SUV_4X4_CROSSOVER",
            "registerNumber": "AA123AA",
            "mileage": 100000
        }
    }
```

## Exemple POSTMAN

- POST https://fkp5n8c5qf.execute-api.eu-west-1.amazonaws.com/dev/scam/detection
- Copier/ coller dans Body->raw 

```json 
{
    "contacts": {
        "firstName": "Christophe",
        "lastName": "Dupont",
        "email": "testdepot222222222222222233333333@yopmail.fr",
        "phone1": {
            "value": "0607080901"
        }
    },
    "creationDate": "2020-01-09T09:02:22.610Z",
    "price": 39000,
    "publicationOptions": ["STRENGTHS", "BOOST_VO"],
    "reference": "B300053623",
    "vehicle": {
        "make": "HONDA",
        "model": "CR-V",
        "version": "IV (2) 1.6 I-DTEC 160 4WD EXCLUSIVE NAVI AT",
        "category": "SUV_4X4_CROSSOVER",
        "registerNumber": "AA123A",
        "mileage": 100000
    }
}
```

Resultat

```json
{
    "reference": "B300053623",
    "scam": true,
    "rules": [
        "rule:email:number_rate"
    ]
}
```


## Deploiement sur AWS

### prérequis:  
- serverless frameworke (si ce n'est pas fait)

```shell
npm install serverless -g
```

- Configurer serverless framework sur votre machine (le role du credential doit couvrir certains privilège deploy: lambda, step functions, ...)

```shell
serverless config credentials --provider aws --key AK**************5R --secret RQ************************Dp
```

- La variable AWS_ACCOUNT (`awsAccount:  ${AWS_ACCOUNT, '9********87'}`)  est obligatoire. Elle permet de générer le ARN de la step function qui sera utilisé dans la conf de la lambda init. 

stateMachineArn est obligatoire pour lancer une state machine.
```javascript
 const params = {
    stateMachineArn: process.env.stateMachineArn,
    input: JSON.stringify(reqBody),
  };
```

Une fois la variable est configurée 

```sh
$ npm i 
$ npm run test
$ sls deploy 
```

Exécution sur l'environement aws :

```shell
$ sls invoke -f init --data '{{event-announcement}}'
```

Exécution des lambdas en local 

```shell
$ sls invoke local -f init --data '{{event-announcement}}'
```

NB: 
La state machine déployée sur AWS qui sera invockée.
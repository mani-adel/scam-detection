const validatorClass = require("../libs/validator");
const validator = new validatorClass();

/**
 *  This function receive event from state machine.
 *  Ex: event = { contacts: { firstName: "Christophe", lastName: "Dupont", email: "testdepot@yopmail.fr", phone1: {  value: "0607080901" } }   }
 *  @param {JSON} event
 *
 *  */
exports.handler = async (event) => {
  const contact = event.contacts;

  const lastNameValidation = validator
    .check(contact.lastName)
    .isLength({ min: 2, attribute: "lastName" })
    .validate();
  const firstNameValidation = validator
    .check(contact.firstName)
    .isLength({ min: 2, attribute: "firstName" })
    .validate();
  const emailValidation = validator
    .check(contact.email)
    .isValidEmail()
    .validate();

  return [...lastNameValidation, ...firstNameValidation, ...emailValidation];
};

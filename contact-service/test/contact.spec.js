const contact = require("../contact");

describe("Testing Contact service", () => {
  test("it should detect scam when firstName length < 2 ", () => {
    const eventContact = {
      contacts: {
        firstName: "A",
        lastName: "Dupont",
        email: "test@yopmail.fr",
        phone1: {
          value: "0607080901",
        },
      },
    };

    const expected = ["rule::firstName::length"];

    return contact.handler(eventContact).then((result) => {
      expect(result).toStrictEqual(expected);
    });
  });

  test("it should detect scam when lastName length < 2 ", () => {
    const eventContact = {
      contacts: {
        firstName: "Christophe",
        lastName: "A",
        email: "test@yopmail.fr",
        phone1: {
          value: "0607080901",
        },
      },
    };

    const expected = ["rule::lastName::length"];

    return contact.handler(eventContact).then((result) => {
      expect(result).toStrictEqual(expected);
    });
  });

  test("it should detect scam when when numeric in email is hight ", () => {
    const eventContact = {
      contacts: {
        firstName: "Christophe",
        lastName: "Dupont",
        email: "adel-127@gmail.com",
        phone1: {
          value: "0607080901",
        },
      },
    };

    const expected = ["rule:email:number_rate"];

    return contact.handler(eventContact).then((result) => {
      expect(result).toStrictEqual(expected);
    });
  });

  test("it should detect scam when total aphanumeric < 70% of prefix length", () => {
    const eventContact = {
      contacts: {
        firstName: "Christophe",
        lastName: "Dupont",
        email: "m-a-n_i-1_2_3@yahoo.fr",
        phone1: {
          value: "0607080901",
        },
      },
    };

    const expected = ["rule:email:alpha_rate"];

    return contact.handler(eventContact).then((result) => {
      expect(result).toStrictEqual(expected);
    });
  });

  test("it should detect scam: firstname, lastname, email ", () => {
    const eventContact = {
      contacts: {
        firstName: "A",
        lastName: "B",
        email: "m-a-n_i-1_2_3@yahoo.fr",
        phone1: {
          value: "0607080901",
        },
      },
    };

    const expected = [
      "rule::lastName::length",
      "rule::firstName::length",
      "rule:email:alpha_rate",
    ];

    return contact.handler(eventContact).then((result) => {
      expect(result).toStrictEqual(expected);
    });
  });
});

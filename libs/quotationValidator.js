const validator = require("./validator");

class quoteValidator extends validator {
  constructor() {
    super(); // Call parent constructor with empty args
  }

  isValidQutation(quotation) {
    if (!quotation) {
      throw new Error("Quotation should not be undefined");
    }

    if (isNaN(this.value) || isNaN(quotation)) {
      throw new Error("Price and Quote shoud be a number");
    }

    const minPrice = quotation - quotation * 0.2;
    const maxPrice = quotation + quotation * 0.2;

    if (!((this.value - minPrice) * (this.value - maxPrice) <= 0)) {
      this.errors.push("rule::price::quotation_rate");
    }
    return this;
  }
}

module.exports = quoteValidator;

const quotationValidator = require("../quotationValidator");
const validator = new quotationValidator();

describe("Testing Quotation Validator ", () => {
  describe("Quotation Validator - Successfull operation", () => {
    const price = 32000;
    const scam = validator.check(price).isValidQutation(35000).validate();
    expect(scam.length).toBe(0);
  });
  describe("Quotation Validator - Failed operation", () => {
    test("validation show scam (quotation_rate)", () => {
      const price = 19000;
      const scam = validator.check(price).isValidQutation(35000).validate();
      expect(scam).toStrictEqual(["rule::price::quotation_rate"]);
    });

    test("it should throw an error when quotation is undefined", () => {
      const price = 19000;
      const expected = new Error("Quotation should not be undefined");
      try {
        validator.check(price).isValidQutation(undefined).validate();
      } catch (error) {
        expect(error).toStrictEqual(expected);
      }
    });

    test("it should throw an error when quotation or price are not a number", () => {
      const price = "AAAAA";
      const quotation = "Quote";
      const expected = new Error("Price and Quote shoud be a number");
      try {
        validator.check(price).isValidQutation(quotation).validate();
      } catch (error) {
        expect(error).toStrictEqual(expected);
      }
    });
  });
});

const validatorClass = require("../validator");
const validator = new validatorClass();

describe("Validator Testing - Firstname & lastename", () => {
  describe("Successfull operation", () => {
    test("should validate the name", () => {
      const input = "Adel";
      const res = validator
        .check(input)
        .isLength({ min: 2, attribute: "firstName" })
        .validate();
      expect(res.length).toBe(0);
    });
  });

  describe("Failed operation", () => {
    test("should return errors when name or first name is short < 2 chars", () => {
      const input = { lastName: "A" };
      const expected = ["rule::lastName::length"];

      const res = validator
        .check(input.lastName)
        .isLength({ min: 2, attribute: "lastName" })
        .validate();
      expect(res).toStrictEqual(expected);
    });

    test("should return entered value in the rule when  attribute is not set and entered value is short < 2 chars", () => {
      const input = { lastName: "A" };
      const expected = ["rule::A::length"];

      const res = validator
        .check(input.lastName)
        .isLength({ min: 2 })
        .validate();
      expect(res).toStrictEqual(expected);
    });

    test("should return errors when name or firstname is undefined", () => {
      const input = undefined;
      const expected = new Error("key/value is not defined");
      try {
        validator
          .check(input)
          .isLength({ min: 2, attribute: "lastName" })
          .validate();
      } catch (error) {
        expect(error).toStrictEqual(expected);
      }
    });

    test("should throw an error when value is not a string", () => {
      const input = { name: "I am an object" };
      const expected = new Error("value should be a string");
      try {
        validator.check(input).isLength({ min: 2 }).validate();
      } catch (error) {
        expect(error).toStrictEqual(expected);
      }
    });

    test("should return errors when value to check  is not a string ", () => {
      const input = "Adel";
      const expected = new Error("value should be a string");

      try {
        validator.check(input).isLength({ min: 2 }).validate();
      } catch (error) {
        expect(error).toStrictEqual(expected);
      }
    });
  });
});

describe("Validator Testing -  Email ", () => {
  describe("email Validator - Failed operation", () => {
    test("email Validator -validation show scam (alpha_rate)", () => {
      const input = "m-a-n_i-1_2_3@yahoo.fr";
      const scam = validator.check(input).isValidEmail().validate();
      expect(scam).toStrictEqual(["rule:email:alpha_rate"]);
    });

    test("email Validator - validation show scam (number_rate)", () => {
      const input = "mani20042020@yahoo.fr";
      const scam = validator.check(input).isValidEmail().validate();
      expect(scam).toStrictEqual(["rule:email:number_rate"]);
    });

    test("should return errors when Email is not a string", () => {
      const input = {};
      const expected = new Error("Email should be a string");
      try {
        validator.check(input).isValidEmail().validate();
      } catch (error) {
        expect(error).toStrictEqual(expected);
      }
    });
  });
});

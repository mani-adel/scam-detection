const validator = require("./validator");

class registrationValidator extends validator {
  constructor() {
    super(); // Call parent constructor with empty args
  }

  isValidRgistrationNumber(conf) {
    if (conf.blackListed) {
      this.errors.push("rule::registernumber::blacklist");
    }
  }
}

module.exports = registrationValidator;

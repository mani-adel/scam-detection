class validator {
  constructor() {
    this.value;
    this.errors = [];
  }

  check(value) {
    // rest object value for each call (same instance can be use to check multiple attributes)
    this.reset();
    if (!value) {
      throw new Error("key/value is not defined");
    }
    this.value = value;
    return this;
  }

  isLength(conf) {
    if (typeof this.value !== "string") {
      throw new Error("value should be a string");
    }
    if (this.value.length < conf.min) {
      this.errors.push(`rule::${conf.attribute || this.value}::length`);
    }
    return this;
  }

  isValidEmail() {
    if (typeof this.value !== "string") {
      throw new Error("Email should be a string");
    }

    const emailPrefix = this.value.split("@")[0];
    const emailPrefixWithOnlyAlphaNum = emailPrefix.replace(/[^a-z0-9]/gi, "");

    if (
      emailPrefix.length > 0 &&
      emailPrefixWithOnlyAlphaNum.length / emailPrefix.length <= 0.7
    ) {
      this.errors.push("rule:email:alpha_rate");
    }

    const emailPrefixWithOnlyNum = emailPrefix.replace(/[^0-9]/gi, "");

    if (
      emailPrefix.length > 0 &&
      emailPrefixWithOnlyNum.length / emailPrefix.length >= 0.3
    ) {
      this.errors.push("rule:email:number_rate");
    }

    return this;
  }

  reset() {
    this.value = undefined;
    this.errors = [];
  }

  validate() {
    return this.errors;
  }
}

module.exports = validator;

exports.handler = async (event) => {
  const reference = event.reference || JSON.parse(event.input).reference;
  const validations =
    event.finalValidation || JSON.parse(event.output).finalValidation;

  const rules = validations
    .map((item) => {
      if (item["resultQuotationValidation"] !== undefined)
        return item["resultQuotationValidation"];
      if (item["resultBlacklistValidation"] !== undefined)
        return item["resultBlacklistValidation"];
      if (item["resultContactValidation"] !== undefined)
        return item["resultContactValidation"];
    })
    .flat();

  const response = {
    reference,
    scam: rules.length > 0 ? true : false,
    rules,
  };
  console.log(response);

  return response;
};

const event = require("./event-build-response");
const buildResponse = require("../buildresponse");

describe("Testing Contact service", () => {
  test("it should detect scam when firstName length < 2 ", () => {
    const expected = {
      reference: "B300053623",
      rules: [
        "rule::lastName::length",
        "rule::firstName::length",
        "rule::price::quotation_rate",
        "rule::registernumber::blacklist",
      ],
      scam: true,
    };

    return buildResponse.handler(event).then((result) => {
      expect(result).toStrictEqual(expected);
    });
  });
});

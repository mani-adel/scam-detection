const AWS = require("aws-sdk");

exports.handler = async (event) => {
  const stepFunctions = new AWS.StepFunctions();
  const reqBody = JSON.parse(event.body);

  const params = {
    stateMachineArn: process.env.stateMachineArn,
    input: JSON.stringify(reqBody),
  };

  return stepFunctions
    .startExecution(params)
    .promise()
    .then(async (data) => {
      await new Promise((r) => setTimeout(r, 3000));
      return stepFunctions
        .describeExecution({ executionArn: data.executionArn })
        .promise();
    })
    .then((result) => {
      console.log(result);
      const finalStateResult = JSON.parse(result.output).finalStateResult;
      return {
        statusCode: 200,
        body: JSON.stringify(finalStateResult),
      };
    })
    .catch((err) => {
      console.error("err: ", err);
      return {
        statusCode: 500,
        body: JSON.stringify({ message: "facing error" }),
      };
    });
};

/*
Example of input
{
        "contacts": {
            "firstName": "C",
            "lastName": "D",
            "email": "testdepot@yopmail.fr",
            "phone1": {
                "value": "0607080901"
            }
        },
        "creationDate": "2020-01-09T09:02:22.610Z",
        "price": 19000,
        "publicationOptions": ["STRENGTHS", "BOOST_VO"],
        "reference": "this is a static ",
        "vehicle": {
            "make": "HONDA",
            "model": "CR-V",
            "version": "IV (2) 1.6 I-DTEC 160 4WD EXCLUSIVE NAVI AT",
            "category": "SUV_4X4_CROSSOVER",
            "registerNumber": "AA123AA",
            "mileage": 100000
        }
    };
 */

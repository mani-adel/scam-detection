const contact = require("../contact-service/contact");
const blacklist = require("../blacklist-service/blacklist");
const quotation = require("../quotation-service/quotation");

const event = {
  contacts: {
    firstName: "Christophe",
    lastName: "Dupont",
    email: "testdepot222222222222222233333333@yopmail.fr",
    phone1: {
      value: "0607080901",
    },
  },
  creationDate: "2020-01-09T09:02:22.610Z",
  price: 19000,
  publicationOptions: ["STRENGTHS", "BOOST_VO"],
  reference: "B300053623",
  vehicle: {
    make: "HONDA",
    model: "CR-V",
    version: "IV (2) 1.6 I-DTEC 160 4WD EXCLUSIVE NAVI AT",
    category: "SUV_4X4_CROSSOVER",
    registerNumber: "AA123AA",
    mileage: 100000,
  },
};

exports.handler = async () => {
  const eventContact = {
    contacts: event.contacts,
  };

  const eventQuotation = {
    price: event.price,
    vehicle: event.vehicle,
  };

  const eventBlacklist = {
    registerNumber: event.vehicle.registerNumber,
  };

  const reference = event.reference;
  const validationContact = contact.handler(eventContact);
  const validationQuotation = quotation.handler(eventQuotation);
  const validationBlacklist = blacklist.handler(eventBlacklist);

  const rules = await Promise.all([
    validationContact,
    validationQuotation,
    validationBlacklist,
  ]).then((results) => results.flat());

  const result = {
    reference,
    scam: rules.length > 0 ? true : false,
    rules,
  };

  console.log("===> ", result);
  return result;
};

this.handler({});

const validatorClass = require("../libs/quotationValidator");
const validator = new validatorClass();

/**
 * ex: event = { price: 19000, vehicle: {"make": "HONDA", "model": "CR-V", ...}}
 * @param {JSON} param
 */
exports.handler = async (event) => {
  const vehicle = event.vehicle;
  const price = event.price;

  return getQuotation(vehicle).then((quotation) =>
    validator.check(price).isValidQutation(quotation).validate()
  );
};

/*eslint no-unused-vars: ["error", { "argsIgnorePattern": "^_" }]*/
/**
 * @param {JSON} vehicle
 */
function getQuotation(_vehicle) {
  return new Promise((resolve, _reject) => {
    setTimeout(() => {
      resolve(35000);
    }, 50);
  });
}

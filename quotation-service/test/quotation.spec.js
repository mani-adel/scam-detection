const quotation = require("../quotation");

describe("Testing quotation service", () => {
  test("it should detect scam when price is 19000 ", () => {
    const eventQuotation = {
      price: 19000,
      vehicle: {
        make: "HONDA",
        model: "CR-V",
        version: "IV (2) 1.6 I-DTEC 160 4WD EXCLUSIVE NAVI AT",
        category: "SUV_4X4_CROSSOVER",
        registerNumber: "AA123AA",
        mileage: 100000,
      },
    };
    const expected = ["rule::price::quotation_rate"];

    return quotation.handler(eventQuotation).then((result) => {
      expect(result).toStrictEqual(expected);
    });
  });

  test("it should validate quotation when price is 32000 ", () => {
    const eventQuotation = {
      price: 32000,
      vehicle: {
        make: "HONDA",
        model: "CR-V",
        version: "IV (2) 1.6 I-DTEC 160 4WD EXCLUSIVE NAVI AT",
        category: "SUV_4X4_CROSSOVER",
        registerNumber: "AA123AA",
        mileage: 100000,
      },
    };
    const expected = [];

    return quotation.handler(eventQuotation).then((result) => {
      expect(result).toStrictEqual(expected);
    });
  });
});
